FROM jupyter/base-notebook:ubuntu-20.04
USER root
WORKDIR /
RUN apt-get update && apt-get install -y \
        dpkg-dev \
        make \
        cmake \
        g++ \
        gcc \
        binutils \
        tar \
        wget \
        libx11-dev \
        libxpm-dev \
        libxft-dev \
        libxext-dev \
        libffi-dev \
        python3-pip \
        libtiff5 \
        libssl-dev \
        libgsl-dev && \
    apt-get clean

RUN python -m pip install --upgrade pip && \
    pip install --no-cache-dir \
        numpy \
        matplotlib \
        scipy

RUN wget https://root.cern/download/root_v6.20.08.Linux-ubuntu20-x86_64-gcc9.3.tar.gz && \
    tar -xzvf root_v6.20.08.Linux-ubuntu20-x86_64-gcc9.3.tar.gz && \
    rm -f root_v6.20.08.Linux-ubuntu20-x86_64-gcc9.3.tar.gz && \
    echo /root/lib >> /etc/ld.so.conf && \
    ldconfig

ENV ROOTSYS /root
ENV PATH $ROOTSYS/bin:$PATH
ENV PYTHONPATH $ROOTSYS/lib:$PYTHONPATH
ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN chown -R ${NB_UID} ${HOME}

WORKDIR ${HOME}

COPY . ${HOME}

EXPOSE 8888

USER ${NB_USER}
