# ANAIS-112 3 years data
# PHYS. REV. D 103, 102005 (2021) 

## Overview

9 detectors 
all detector has same mass, energy threshold and live time
* MASS of every detector: 12.5 kg
* Energy threshold: 1 keV (*)
* Live time temporal distribution: see file ANAIS112liveTime_3y_10days.csv
* Total live time: 1013.83 days (**)
* Energy resolution from 0 to 25 keV
calculated for the summ of all detectors during the first month of data taking:  
 $`\sigma = (-0.008 \pm 0.001) + (0.378 \pm 0.002)\times \sqrt{\rm{E(keV)}}`$ [Eur. Phys. J. C (2019) 79:228](https://doi.org/10.1140/epjc/s10052-019-6697-4)

Notes:
 - (*) all energies are given in keVee. For simplicity we use keV symbol for keVee
 - (**) The last 10-days time bin of the third year of data taking (bin 111, live time: 4.74 days) 
was not completed, and therefore not used in the data analysis of PRD103,102005(2021) 
but in section III the live time of the experiment was incorrectly quoted as 1013.83+4.74=1018.6 days instead of 1013.83 days.
This typo does not affect the subsequent analysis.


**********************
**********************
## Event Data:
--------------------
Experimental bkg in counts/kg/day (corrected by efficiency and live time)

ANAIS112_3y_10days_1keV_6keV_D?.csv -> bkg vs time for every detector(0-9) integrated in the energy region [1-6] keV in 10-days bins

ANAIS112_3y_10days_2keV_6keV_D?.csv -> bkg vs time for every detector(0-9) integrated in the energy region [2-6] keV in 10-days bins

Format:  
 bin_center(days) , bkg(counts/kg/d) , error(counts/kg/d)

**********************
**********************
## Background Data:
--------------------
MC simulated bkg in counts/kg/day

ANAIS112bkg_1keV_6keV_D?.csv -> MC simulated background vs time for every detector(0-9) integrated in the energy region [1-6] keV in 15-days bins

ANAIS112bkg_2keV_6keV_D?.csv -> MC simulated background vs time for every detector(0-9) integrated in the energy region [2-6] keV in 15-days bins

Format:  
 bin_center(days) , bkg(counts/kg/d)

**********************
**********************
## Efficiency
--------------------
ANAIS112eff_3y_D?.csv -> efficiency vs energy for every detector(0-9) from 1-6 keV

Format:  
 bin_center(keV) , efficiency , error

**********************
**********************
## Live Time
--------------------
Live time in days for every 10-days bin
ANAIS112liveTime_3y_10days.csv

Format:  
 live_time(days)


**********************
**********************
## Chi2 minimization according to PHYS. REV. D 103, 102005 (2021) equations (2) and (6)
---------------------
a112modFit.C  
fitting root macro (roofit)  
ROOT Version: 6.19/02

perform the chi2 minimization according to PHYS. REV. D 103, 102005 (2021) equations (2) and (6) 

output: figures (13) and (14)

usage:  
a112modFit(int eneI, bool useMC=1, bool phaseFree=0)

input parameters:

* eneI: initial energy. Possible values:  
  1 -> fit [1-6] keV (figure 13)
  2 -> fit [2-6] keV (figure 14)

* useMC: background model  
  1 (default) -> use MC background model (equation 6)
  0 -> use single exponential approximation for background (this result is not included in PRD103,102005(2021) )

* phaseFree: fix/free phase parameter  
  0 (default) -> phase fixed to 2nd June
  1 -> perform a phase free analysis  
  ** be aware that in this case the fit is biased , see details in PHYS. REV. D 103, 102005 (2021)
  output: simple version of figure (17)
